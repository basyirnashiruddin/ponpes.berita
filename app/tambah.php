<?php 
	include 'functions/query.php';
	include 'header.php';

	if (isset($_POST['judul'])) {
		input_berita($_POST['judul'], $_POST['isi']);
	}



 ?>

<form action="tambah.php" method="POST">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputJudul">Judul</label>
      <input type="text" class="form-control" id="inputJudul" name="judul" placeholder="Judul" required="">
    </div>
  </div>
  <div class="form-group">
    <label for="inputIsi">Isi</label>
    <textarea name="isi" class="form-control" id="inputIsi" name="isi"></textarea>
  </div>
  </div>
  <button type="submit" class="btn btn-primary">Tambah</button>
</form>
