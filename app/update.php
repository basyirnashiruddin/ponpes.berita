<?php 
	include 'functions/query.php';
	include 'header.php';

	if (isset($_POST['judul'])) {
		update_berita($_GET['id'], $_POST['judul'], $_POST['isi']);
	}

  $berita = get_one_berita($_GET['id']);
  if (mysqli_num_rows($berita) > 0) {
      $row = mysqli_fetch_assoc($berita);
      $data = $row;
  } else {
      echo "0 results";
  }

 ?>

<form action="update.php?id=<?php echo $_GET['id'] ?>" method="POST">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputJudul">Judul</label>
      <input type="text" class="form-control" id="inputJudul" name="judul" placeholder="Judul" value="<?php echo $data['judul'] ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="inputIsi">Isi</label>
    <textarea name="isi" class="form-control" id="inputIsi" name="isi">
      <?php echo $data['isi'] ?>
    </textarea>
  </div>
  </div>
  <button type="submit" class="btn btn-primary">Update</button>
</form>
