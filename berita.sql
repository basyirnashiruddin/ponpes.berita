-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 17, 2019 at 06:08 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `berita`
--

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `tgl_terbit` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id`, `judul`, `isi`, `tgl_terbit`) VALUES
(1, 'Kenapa Ibu Kota Baru Bukan di Jawa atau Sumatera?', 'Jakarta - Masih ada pihak yang belum puas dengan keputusan pemerintah memindahkan ibu kota negara (IKN) ke Kalimantan Timur (Kaltim). Kaltim dipilih langsung oleh Presiden Joko Widodo (Jokowi).\r\n\r\nKetua Pansus Pemindahan Ibu Kota Zainudin Amali bahkan menilai kajian Bappenas soal pemindahan ibu kota ke Kaltim belum lengkap.\r\n\r\nTerlepas dari itu, Ketua Tim Komunikasi Ibu kota Negara (IKN)/Sekretaris Menteri PPN/Bappenas Hirmawan Hariyoga Djojokusumo menegaskan bahwa rencana pemindahan ibu kota negara bukan sebuah rencana yang tiba-tiba. Sudah dilakukan kajian sejak 2017.', '2019-10-17'),
(2, 'Sederet Kasus Korupsi yang Bikin Jajaran PUPR Dicokok KPK', 'Jakarta - Kasus korupsi terbongkar pada pembangunan jalan nasional di Kalimantan Timur. KPK mencokok Kepala Badan Jalan Nasional (BPJN) Wilayah XII Kalimantan Timur Refly Ruddy Tangkere.\r\n\r\nKPK menduga ada suap mengalir ke pembangunan jalan nasional di Kalimantan Timur. Kementerian PUPR sendiri mengaku terkejut, namun tetap akan kooperatif mendukung KPK menyelesaikan kasus korupsi jajarannya.\r\n\r\nJauh sebelum itu, Kementerian PUPR sendiri memang sering terganjal kasus korupsi. Sepanjang 2019 KPK terus mengembangkan penyelidikan pada kasus korupsi SPAM. Berikut ini, beberapa kasus korupsi yang pernah menyandung PUPR. (eds/eds)', '2019-10-17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
